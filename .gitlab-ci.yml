# References:
# - https://docs.gitlab.com/ee/ci/yaml
# - https://docs.gitlab.com/ee/ci/variables/README.html#list-all-environment-variables

# Gitlab doesn't have a runner tagged with shell
# You need to register you PC as a runner
# - https://docs.gitlab.com/runner/install/
# - https://docs.gitlab.com/runner/register/index.html

stages:
  - install

  # Build & tests jobs have a similar execution time
  # Better execute them in parallel
  - build_and_test
  - deploy

# Global config for all jobs
# Could be set in default section, personnal choice here
variables:
  PROJECT_PATH: "$CI_PROJECT_DIR"
  OUTPUT_PATH: "$CI_PROJECT_DIR/artifacts"
  APP_OUTPUT_PATH: "$CI_PROJECT_DIR/artifacts/app"
  APP_GITLAB_OUTPUT_PATH: "$CI_PROJECT_DIR/artifacts/app-gitlab"
  DOCKER_IMAGE_NAME: "$CI_REGISTRY_IMAGE/app"
  DOCKER_IMAGE_TAG: "$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
  IS_RELEASE_PIPELINE: "$CI_COMMIT_TAG"

# Configuration shared for all jobs
default:
  # It saves ~10 sec to pull image from project container registry
  # instead of the default docker.io registry
  image: $CI_REGISTRY_IMAGE/ci-node:latest
  tags:
    - docker

# Define an hidden job to be used with extends
# Better than default to avoid activating cache for all jobs
.dependencies_cache:
  cache:
    key:
      files:
        - package-lock.json
    paths:
      - node_modules
    policy: pull

# Projects jobs definition
install_dependencies:
  stage: install
  script:
    - npm install

    # Compile angular dependencies for ivy before next step
    # So only this step has to push the cache
    #- npm run ngcc --properties es2015 --create-ivy-entry-points

  # Redefine cache to have default pull-push policy
  extends: .dependencies_cache
  cache:
    policy: pull-push
  #only:
  #  changes:
  #    - package-lock.json

test_app:
  stage: build_and_test

  # Use custom image because installing chrome in before_scripts takes ~30 sec
  image: $CI_REGISTRY_IMAGE/ci-tests:latest
  script:

    # Could be in a parallel job during this stage but:
    # - it would use an additional runner for a ~8 sec task
    # - the additionnal job would load docker executor and cache (~25 sec)
    # - the pipeline will fail only after build_app job is over
    #
    # Drawbacks for including lint in this stage:
    # - this job is ~8 sec longer
    # - when the job fail it can be due to either the lint or build
    - npm run lint
    - npm run test:ci

    # Use package to have the average of coverage metrics output by unit tests
    - coverage-average $OUTPUT_PATH/coverage/text-summary.txt

  # Collects coverage to display in MR an job results
  coverage: '/Coverage average is \d+.\d+%/'
  artifacts:
    name: "tests-and-coverage"
    reports:
      junit:
        - $OUTPUT_PATH/tests/junit-test-results.xml
      cobertura:
        - $OUTPUT_PATH/coverage/cobertura-coverage.xml

  # Avoids all pipeline artifacts to be fetched
  dependencies: []
  extends: .dependencies_cache

build_app:
  stage: build_and_test
  script:
    - npm run build
  after_script:
    - cp $PROJECT_PATH/nginx.conf $APP_OUTPUT_PATH
    - cp $PROJECT_PATH/Dockerfile $APP_OUTPUT_PATH
  artifacts:
    name: "sirh-angular-frontend"
    paths:
      - $APP_OUTPUT_PATH
  extends: .dependencies_cache

build_app_gitlab:
  stage: build_and_test
  script:
    - npm run build:gitlab --  --outputPath $APP_GITLAB_OUTPUT_PATH
  after_script:
    # https://angular.io/guide/deployment#deploy-to-github-pages
    - cp $APP_GITLAB_OUTPUT_PATH/index.html $APP_GITLAB_OUTPUT_PATH/404.html
  artifacts:
    name: "sirh-angular-frontend-gitlab"
    paths:
      - $APP_GITLAB_OUTPUT_PATH
  extends: .dependencies_cache

# Deployment steps


# Prod deploys to GitLab Pages
# - https://docs.gitlab.com/ee/ci/yaml/#pages
# - https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html
pages:
  stage: deploy
  script:
    - mv $APP_GITLAB_OUTPUT_PATH $PROJECT_PATH/public
  artifacts:
    paths:
      - public
  dependencies:
    - build_app_gitlab

  # Flags this job to let known GitLab it's
  # deploying on prod environment
  # Setup environments in Operations > Environments
  environment:
    name: staging-gitlab
    url: https://kreezus.gitlab.io/sirh/angular-frontend

  # Not runned automatically by the pipeline
  # Requires to manually start the job in the UI
  when: manual
  #only:
  #  - tags

# Updates the CI images defined on .ci folder multi-stage Dockerfile
#
# It's possible to push an official image to the project docker registry
# without using this step and the multi-stage Dockerfile:
# - docker pull node:12-alpine
# - docker tag node:12-alpine registry.gitlab.com/jbardon/angular-app-pipeline/ci-node:latest
# - docker login registry.gitlab.com
# - docker push registry.gitlab.com/jbardon/angular-app-pipeline/ci-node:latest
.update_ci_images:

  # Special stage which ensure this job is run first
  stage: .pre
  tags:
    - shell
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - cd $PROJECT_PATH/.ci
  script:
    - docker build --tag $CI_REGISTRY_IMAGE/$STAGE_IMAGE:latest
                   --target $STAGE_IMAGE $PROJECT_PATH/.ci

    - docker push $CI_REGISTRY_IMAGE/$STAGE_IMAGE:latest

  # Runs this job for each given parameter value in parallel
  # - https://docs.gitlab.com/ee/ci/yaml/#parallel-matrix-jobs
  parallel:
    matrix:
      - STAGE_IMAGE: [ci-node, ci-tests]
  only:
    changes:
      - .ci/Dockerfile
