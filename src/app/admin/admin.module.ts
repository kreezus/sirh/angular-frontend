import { ProfileModule } from './../profile/profile.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { UsersListComponent } from './users-list/users-list.component';

@NgModule({
  declarations: [AdminComponent, UsersListComponent],
  imports: [CommonModule, AdminRoutingModule, ProfileModule],
})
export class AdminModule {}
