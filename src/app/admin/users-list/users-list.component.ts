import { UserService } from './../../core/user.service';
import { IUser } from './../../models/user';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css'],
})
export class UsersListComponent implements OnInit {
  users$: Observable<IUser[]>;

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.refreshList();
  }

  refreshList(): void {
    this.users$ = this.userService.findAll();
  }

  deleteUser(user: IUser): void {
    console.log('Delete user: ', user);
  }
}
