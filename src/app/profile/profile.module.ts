import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';

@NgModule({
  declarations: [ProfileComponent, UpdatePasswordComponent],
  imports: [CommonModule, ProfileRoutingModule, FormsModule],
  exports: [ProfileComponent],
})
export class ProfileModule {}
