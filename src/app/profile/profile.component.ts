import { IUser, User } from './../models/user';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  user: IUser = new User();

  constructor() {}

  ngOnInit(): void {}

  updateUserInfo(user: IUser): void {
    console.log('Updating user infos: ', user);
  }
}
