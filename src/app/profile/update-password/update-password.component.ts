import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css'],
})
export class UpdatePasswordComponent implements OnInit {
  password = '';
  newPassword = '';
  newPasswordConfirm = '';
  constructor() {}

  ngOnInit(): void {}

  updatePassword(passwordInfos): void {
    console.log('Updating password: ', passwordInfos);
  }
}
