import { IUser, User } from './../models/user';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  initialUsers: IUser[] = [
    new User(
      1,
      'Clément',
      'Dubois',
      'clement.dubois@exemple.com',
      null,
      new Date()
    ),
    new User(
      2,
      'Patrick',
      'Sebastien',
      'patrick.sebastien@exemple.com',
      null,
      new Date()
    ),
  ];

  constructor() {}

  findAll(): Observable<IUser[]> {
    const users = of(this.initialUsers).pipe(delay(2000));
    return users;
  }

  registerUser(newUser: IUser): void {
    this.initialUsers.push(newUser);
  }

  removePet(firstName: string): void {
    this.initialUsers.splice(
      this.initialUsers.findIndex((u) => u.firstName === firstName),
      1
    );
  }
}
