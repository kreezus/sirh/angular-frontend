import { IUser, User } from './../../models/user';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  newUser: IUser = new User();
  confirmPassword = '';

  constructor() {}

  ngOnInit(): void {}

  register(newUser: any): void {
    console.log('Registering user: ', newUser);
  }
}
