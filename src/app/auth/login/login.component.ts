import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginInfo: any = {};
  constructor() {}

  ngOnInit(): void {}

  login(loginInfo: any): void {
    console.log('Login info: ', loginInfo);
  }
}
