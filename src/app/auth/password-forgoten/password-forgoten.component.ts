import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-password-forgoten',
  templateUrl: './password-forgoten.component.html',
  styleUrls: ['./password-forgoten.component.css'],
})
export class PasswordForgotenComponent implements OnInit {
  email = '';
  error: string;
  constructor() {}

  ngOnInit(): void {}
  recoverPassword(email: string): void {
    console.log('Request to recover password for email: ', email);
  }
}
