export interface IUser {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  birthDay: Date;
  avatarUrl: string;
}
export class User implements IUser {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  birthDay: Date;
  avatarUrl: string;
  constructor(
    id?: number,
    firstName?: string,
    lastName?: string,
    email?: string,
    password?: string,
    birthDay?: Date,
    avatarUrl?: string
  ) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.birthDay = birthDay || new Date();
    this.avatarUrl = avatarUrl || 'https://via.placeholder.com/150';
  }
}
